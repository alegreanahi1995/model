package User3;


import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import modelo.Lugar;
import modelo.SimpleCacheManager;
import modelo.Tiempo;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
public class  CA1 {

    @Test public void Cache_devolverTiempoExistente() throws ClientProtocolException, URISyntaxException, IOException {
      SimpleCacheManager cache =  SimpleCacheManager.getInstance();
      Lugar lugar=new Lugar( "Argentina","Buenos Aires","Garin");
      Calendar fecha= Calendar.getInstance();
      fecha.set(2021, 4, 10);


      Tiempo t=new Tiempo();
      t.setConditions("light rain");
      t.setFecha(10, 04, 2021);
      t.setLocalidad("Garin");
      t.setMaxt("25.76");
      t.setPais("Argentina");
      t.setProvincia("Buenos Aires");
      t.setTemp("25.76");
      cache.put(lugar, fecha, t);
      assertTrue(cache.getTiempo(new Lugar("Argentina","Buenos Aires","Garin") ,fecha).getConditions().equals("light rain"));
      
      assertTrue(cache.getTiempo(new Lugar("Argentina","Buenos Aires","Garin") ,fecha).getTemp().equals("25.76"));
      		  
    }
}

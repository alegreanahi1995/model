package User3;


import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Test;

import modelo.Lugar;
import modelo.NullTiempo;
import modelo.SimpleCacheManager;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
public class  CA2 {


    @Test public void Cache_devolverTiempoNoExistente() throws ClientProtocolException, URISyntaxException, IOException {
     
    	SimpleCacheManager cache =  SimpleCacheManager.getInstance();
      
      Calendar fecha= Calendar.getInstance();
      fecha.set(2021, 03, 1);
      
  assertTrue(cache.getTiempo(new Lugar("Argentina","Buenos Aires","Garin"), fecha) instanceof NullTiempo);
    		  
   }
 
}

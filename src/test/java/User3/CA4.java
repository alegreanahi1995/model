package User3;


import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import modelo.AbstractTiempo;
import modelo.Lugar;
import modelo.NullTiempo;
import modelo.SimpleCacheManager;
import modelo.Tiempo;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
public class  CA4 {

    @Test public void Cache_Agregar() throws ClientProtocolException, URISyntaxException, IOException {
    	  SimpleCacheManager cache =  SimpleCacheManager.getInstance();
          Lugar lugar=new Lugar( "Argentina","Buenos Aires","Garin");
          Calendar fecha= Calendar.getInstance();
          fecha.set(2021, 4, 10);
          Tiempo t=new Tiempo();
          t.setConditions("light rain");
          t.setFecha(10, 04, 2021);
          t.setLocalidad("Garin");
          t.setMaxt("25.76");
          t.setPais("Argentina");
          t.setProvincia("Buenos Aires");
          t.setTemp("25.76");
          cache.put(lugar, fecha, t);
          
          AbstractTiempo tiempodevuelto=cache.getTiempo(lugar, fecha);
         
         assertTrue(tiempodevuelto.getTemp()==t.getTemp());

         assertTrue(tiempodevuelto.getMaxt()==t.getMaxt());

         assertTrue(tiempodevuelto.getConditions()==t.getConditions());
         
    		  
    }
}

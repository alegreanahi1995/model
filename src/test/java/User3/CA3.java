package User3;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import modelo.Lugar;
import modelo.NullTiempo;
import modelo.SimpleCacheManager;
import modelo.Tiempo;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
public class  CA3 {

	//luego de 3 elementos se vacia la cache y vuelve a iniciarse
    @Test public void Cache_vaciar() throws ClientProtocolException, URISyntaxException, IOException {
    	  SimpleCacheManager cache =  SimpleCacheManager.getInstance();
          Lugar lugar=new Lugar( "Argentina","Buenos Aires","Garin");
          Calendar fecha= Calendar.getInstance();
          fecha.set(2021, 4, 10);
          Tiempo t=new Tiempo();
          t.setConditions("light rain");
          t.setFecha(10, 04, 2021);
          t.setLocalidad("Garin");
          t.setMaxt("25.76");
          t.setPais("Argentina");
          t.setProvincia("Buenos Aires");
          t.setTemp("25.76");
          cache.put(lugar, fecha, t);
          
          
          
          lugar=new Lugar( "Argentina","Buenos Aires","Don Torcuato");
          fecha= Calendar.getInstance();
          fecha.set(2021, 4, 10);
          t=new Tiempo();
          t.setConditions("light rain");
          t.setFecha(10, 04, 2021);
          t.setLocalidad("Don torcuato");
          t.setMaxt("23");
          t.setPais("Argentina");
          t.setProvincia("Buenos Aires");
          t.setTemp("23");
          cache.put(lugar, fecha, t);
          


          lugar=new Lugar( "Argentina","Buenos Aires","Mar del Plata");
          fecha= Calendar.getInstance();
          fecha.set(2021, 4, 10);
          t=new Tiempo();
          t.setConditions("clouds");
          t.setFecha(10, 04, 2021);
          t.setLocalidad("Mar del Plata");
          t.setMaxt("16");
          t.setPais("Argentina");
          t.setProvincia("Buenos Aires");
          t.setTemp("16");
          
          cache.put(lugar, fecha, t);
          
        

          lugar=new Lugar( "Argentina","Buenos Aires","Miramar");
          fecha= Calendar.getInstance();
          fecha.set(2021, 4, 10);
          t=new Tiempo();
          t.setConditions("clouds");
          t.setFecha(10, 04, 2021);
          t.setLocalidad("Miramar");
          t.setMaxt("16");
          t.setPais("Argentina");
          t.setProvincia("Buenos Aires");
          t.setTemp("16");
          cache.put(lugar, fecha, t);
         assertTrue(cache.tamanio()==1);
    		  
    }
}

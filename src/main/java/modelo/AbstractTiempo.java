package modelo;

public abstract class AbstractTiempo {

		protected int dia;
		protected int mes;
		protected int anio;
		protected Lugar lugar;
		protected String address;
		protected String temp;
		protected String maxt;
		protected String conditions;
		
		public abstract void setFecha(int dia,int mes, int anio);
		
		public abstract int getDia();
		
		public abstract int getMes();
		
		public abstract int getAnio();
		
		public abstract String getAddress() ;
		public abstract void setAddress(String address) ;
		public abstract String getTemp();
		public abstract void setTemp(String temp);
		public abstract String getMaxt();
		public abstract void setMaxt(String maxt);

		public abstract String getConditions();
		public abstract void setConditions(String conditions);
		public abstract String getPais();
		public abstract void setPais(String pais);
		public abstract String getProvincia() ;
		public abstract void setProvincia(String provincia);
		public abstract String getLocalidad();
		
		public abstract void setLocalidad(String localidad);
		
		@Override
		public abstract String toString();
}

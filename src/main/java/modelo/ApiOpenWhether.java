package modelo;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class ApiOpenWhether extends Api {

       String API_KEY = "988ebbfc0bc3f8b2039b9ec2f4f53b33";
	    
	    	
	    public ApiOpenWhether()
	    {
	    	this.nombre="ApiOpenWhether";
	    	this.url=" www.openweathermap.org";
	    }
	   public AbstractTiempo getTiempo(Lugar l, Calendar fecha) throws ClientProtocolException, URISyntaxException, IOException {

			ApiOpenWhether api=new ApiOpenWhether();
			
			String json=JsonClimaOpenWhater(fecha,l.getLocalidad(),l.getProvincia(),l.getPais());
			
			
			Gson gson=new Gson();
			
			int dia,mes,anio;
			Tiempo t=gson.fromJson(json, Tiempo.class);
			dia=fecha.get(Calendar.DAY_OF_MONTH);
		  	mes=fecha.get(Calendar.MONTH);
		  	anio=fecha.get(Calendar.YEAR);
			t.setFecha(dia,mes,anio);
			return t;
	    	}


	 private static Map<String,Object> jsonToMap(String str){
	        Map<String,Object> map = new Gson().fromJson(str,new 
	    TypeToken<HashMap<String,Object>> () {}.getType());
	        return map;
	 }
	    
	private String JsonClimaOpenWhater(Calendar fecha,String localidad, String provincia, String pais) {
	    StringBuilder result = new StringBuilder();
	    String LOCATION = pais + "," + provincia + ", BUE";
	    String urlString = "http://api.openweathermap.org/data/2.5/weather?q=" + LOCATION + "&appid=" + API_KEY + "&units=metric";
	    
		  try{		    
		        URL url = new URL(urlString);
		        URLConnection conn = url.openConnection();
		        BufferedReader rd = new BufferedReader(new InputStreamReader (conn.getInputStream()));
		        String line;
		        
		        while ((line = rd.readLine()) != null){
		            result.append(line);
		        }

		        rd.close();
            
		    }catch (IOException e){
		        System.out.println(e.getMessage());
		    }
			 JSONObject jsonWeatherForecast = null;
			 jsonWeatherForecast = new JSONObject(result.toString());
			 
			 JSONArray weather=jsonWeatherForecast.getJSONArray("weather");
			 JSONObject forecastValue = weather.getJSONObject(0);
			 
			 
			 Map<String, Object > respMap = jsonToMap (result.toString());
		     Map<String, Object > mainMap = jsonToMap (respMap.get("main").toString());
	
		     String jsonComplejo="{'Pais':'" + pais + "', 'Provincia':'" + provincia + "', 'Localidad':'" + localidad
		 		+ "', 'temp':'" + mainMap.get("temp").toString() + "', 'maxt':'" + mainMap.get("temp_max").toString()
		 				+"', 'conditions':'" + forecastValue.getString("description") + "' }";
		 		  System.out.print(result.toString());
	

		  
		   return jsonComplejo.toString();
          

	}
	 

	

}
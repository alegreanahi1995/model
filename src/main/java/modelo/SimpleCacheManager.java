package modelo;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

public class SimpleCacheManager implements ServicioClima{

       private static SimpleCacheManager instance;
       private static Object monitor = new Object();
       private Map<String, Object> cache = Collections.synchronizedMap(new HashMap<String, Object>());
       private static PoliticaCache poli_cache;
       AbstractTiempo tiempo = new Tiempo();
       
       private SimpleCacheManager() {
       }
       
       
       public int tamanio()
       {
    	   return cache.size();
       }

       public void put(Lugar l, Calendar fecha,Object value) {
    	   
    	   int dia,mes,anio;
      		//Calendar hoy=Calendar.getInstance();
      		dia=fecha.get(Calendar.DAY_OF_MONTH);
      		mes=fecha.get(Calendar.MONTH);
      		anio=fecha.get(Calendar.YEAR);
      		String clave =l.getPais()+","+l.getProvincia()+","+l.getLocalidad()+" | "+dia+","+mes+","+anio;
           
      		eliminarElementosViejos();
      		
      	    if (!SimpleCacheManager.getInstance().containsKey(clave)) { 
      	    	poli_cache.eliminarcache(instance);
              cache.put(clave, value);
      	    }
      	    
    	
         
       }
       
       
       
       

       public boolean containsKey(String cacheKey) {
           return cache.containsKey(cacheKey);
         
       }

    

       public void clear(String cacheKey) {
           cache.put(cacheKey, null);
       }

       public void clear() {
           cache.clear();
       }

       public static SimpleCacheManager getInstance() {
           if (instance == null) {
               synchronized (monitor) {
                   if (instance == null) {
                       instance = new SimpleCacheManager();
                       poli_cache=new PoliticaCache1();
                   }
               }
           }
           return instance;
       }

       
   	public void eliminarElementosViejos()
   	{
   		
   		Calendar hoy=Calendar.getInstance();
   		Date fechaactual=new Date();
		SimpleDateFormat simple=new SimpleDateFormat("dd/MM/YYYY");
		String fechastring=simple.format(fechaactual);
		

		String[] fecha = fechastring.split("/");
		int dia = Integer.parseInt(fecha[0].toString()); 
		int mes = Integer.parseInt(fecha[1].toString()); 
		int anio=Integer.parseInt(fecha[2].toString()); 
	
		hoy.set(anio, mes,dia);
			
for (Map.Entry<String, Object> entry : cache.entrySet()) {

	String[] parts = entry.getKey().split(" | ");
	String cadena1 = parts[0]; //
	String cadena2 = parts[1]; // 
	if(entry.getKey().compareTo(cadena1+" | "+dia+","+mes+","+anio)==0)
	{

		cache.remove(entry.getKey());
	}
	
}

   	}

	
   public AbstractTiempo getTiempo(Lugar l, Calendar fecha) throws ClientProtocolException, URISyntaxException, IOException 
   {
	    int dia,mes,anio;
 		 dia=fecha.get(Calendar.DAY_OF_MONTH);
  		mes=fecha.get(Calendar.MONTH);
  		anio=fecha.get(Calendar.YEAR);
  		
 		String cacheKey =l.getPais()+","+l.getProvincia()+","+l.getLocalidad()+" | "+dia+","+mes+","+anio;
    	
 	eliminarElementosViejos();


    	   if(this.containsKey(cacheKey)) {
            return (Tiempo) cache.get(cacheKey);
    	   }
    	   

    		Api app = new Api();
           	try {

				tiempo =app.getTiempo(l,fecha);

			} catch (URISyntaxException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
           	
           	
           	if(tiempo instanceof NullTiempo) return new NullTiempo();
           	
           	SimpleCacheManager.getInstance().put(l, fecha,tiempo);
           	return tiempo;
       }



}

package modelo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.apache.http.client.ClientProtocolException;



public interface ServicioClima {


	   public AbstractTiempo getTiempo(Lugar l, Calendar c) throws ClientProtocolException, URISyntaxException, IOException ;
	
}

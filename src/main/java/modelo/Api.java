package modelo;


import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;


public class Api  implements ServicioClima{



//	   private String estado;
	   protected String nombre;
	   protected String url;
		
	   protected String estado;
		
	
	
		public AbstractTiempo getTiempo(Lugar l,Calendar c) throws ClientProtocolException, URISyntaxException, IOException {
			
			List<Api> lista=null;
			try {
				lista = this.getServiciosFuncionan(this.getServicios());
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(lista.size()==0) return new NullTiempo();
			
			if(lista.size()>0) {
			
				for (Api api : lista) {
					AbstractTiempo tiempo=api.getTiempo(l, c);
					if(!(tiempo instanceof NullTiempo))
			return api.getTiempo(l, c);
					
				}
			}
			return new NullTiempo();
		}
		
	public static List<Api> getServiciosFuncionan(List<Api> lista) throws IOException, InterruptedException {

			
			
			List <Api> apis=new ArrayList<Api>();
			Set<Api> objeto;
			
				for (Api api : lista) {
					if(api.getEstado().equals("Con conexion"))
					{

						apis.add(api);
					}
					 }
			return apis;
			}
			
		
		public static List<Api> getServicios() throws IOException, InterruptedException {

			
			
			List <Api> apis=new ArrayList<Api>();
			Set<Api> objeto;
			try {
				String direcdestino=new File(".").getAbsolutePath().substring(0,new File(".").getAbsolutePath().length()-1)+"\\src\\main\\java\\apis\\";

				System.out.print("HELLO");


				objeto = findClasses(direcdestino);
				

				for (Api cadena : objeto) {
					/*if(cadena.getEstado().equals("Con conexion"))
					{

						apis.add(cadena);
					}
						*/
					apis.add(cadena);
					 }
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return apis;
			}
			
		
		
		private static Set<Api> findClasses(String path) throws InstantiationException, IllegalAccessException {
			Set<Api> result = new HashSet<>();
			for (File f : new File(path).listFiles()) {
				
		//	if (!f.getName().endsWith(".class") )continue;
			//Class c;
			/*try {
				c = Class.forName(f.getName());
				
				if (!c.isInstance(interafaz.class))
					throw new RuntimeException();
					result.add( (interafaz) c.newInstance());
					}
			
			 catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
				Api i;
				try {
					i = (Api) Class.forName("apis."+f.getName().substring(0, f.getName().length()-5)).newInstance();
				
					result.add(i);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			return result;
			}


			public String getEstado() throws IOException, InterruptedException {
				Runtime runtime = Runtime.getRuntime();
				Process proc = runtime.exec("ping "+url); 

				int mPingResult = proc .waitFor();
				if(mPingResult == 0){
				       return "Con conexion";
				}else{
				       return "Sin conexion";
				}
			}

		



	

}

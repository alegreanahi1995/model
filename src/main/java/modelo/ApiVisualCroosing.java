package modelo;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Map;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

public class ApiVisualCroosing extends Api{


	  private String API_KEY = "1PYNQ6AWUDJE9AFERDCHJHSXK";
	
	  public ApiVisualCroosing()
	  {
		  this.nombre="ApiVisualCroosing";
		  this.url="www.visualcrossing.com";
	  }
	  
	public AbstractTiempo getTiempo(Lugar l, Calendar fecha) throws ClientProtocolException, URISyntaxException, IOException {

		ApiVisualCroosing api=new ApiVisualCroosing();
	
		String tiempo=api.getTiempo(fecha,l.getLocalidad(),l.getProvincia(),l.getPais());
		
		Gson gson=new Gson();
		
		int dia,mes,anio;
		Tiempo t=gson.fromJson(tiempo, Tiempo.class);
		dia=fecha.get(Calendar.DAY_OF_MONTH);
	  	mes=fecha.get(Calendar.MONTH);
	  	anio=fecha.get(Calendar.YEAR);
		t.setFecha(dia,mes,anio);
		return t;
			
	}

		private String getTiempo(Calendar c,String localidad,String provincia,String pais) throws URISyntaxException, ClientProtocolException, IOException {
			String LOCATION = pais + "," + provincia + ", BUE";
			  String rawResult = "";
			  
			URIBuilder builder = new URIBuilder("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/weatherdata/forecast");

			   builder.setParameter("aggregateHours", "24")
			   	.setParameter("contentType", "json")
			   	.setParameter("locationMode", "single")
			   	.setParameter("unitGroup", "metric")
			   	.setParameter("key", API_KEY)
			   	.setParameter("locations", LOCATION);
			   JSONObject jsonWeatherForecast = null;
			   HttpGet get = new HttpGet(builder.build());
			   CloseableHttpClient httpclient = HttpClients.createDefault();
			   CloseableHttpResponse response = httpclient.execute(get);    


			   try {
			    if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			    System.out.printf("Bad response status code:%d%n", response.getStatusLine().getStatusCode());
			    return "Error";
			    }
			    
			    HttpEntity entity = response.getEntity();
			       if (entity != null) {
			        rawResult=EntityUtils.toString(entity, Charset.forName("utf-8"));
	             }
			        
			   } finally {
			    response.close();
			   }  
		   		   
			return rawResult.toString();

		}
		
		

		private Tiempo JsonClimaVisualCrossing(Calendar c,String JsonClima,String pais, String provincia,String localidad) {
			 Tiempo clima = new Tiempo();
			
			 JSONObject jsonWeatherForecast = null;
			 jsonWeatherForecast = new JSONObject(JsonClima);
			 
			 JSONObject location=jsonWeatherForecast.getJSONObject("location");
			 
			 JSONArray values=location.getJSONArray("values");
			 JSONObject forecastValue = values.getJSONObject(1);
			   double humidity=forecastValue.getDouble("humidity");
			   double maxt=forecastValue.getDouble("maxt");
			   double precip=forecastValue.getDouble("precip");
			   double temp=forecastValue.getDouble("temp");
			   
			/* clima.setAddress(location.getString("address")); 		 
			 clima.setConditions(forecastValue.getString("conditions"));
			 clima.setHumidity(String.valueOf(humidity));
			 clima.setMaxt(String.valueOf(maxt));
			 clima.setPrecip(String.valueOf(precip));
			 clima.setTemp(String.valueOf(temp));*/

			return clima;
		}
		
		


}

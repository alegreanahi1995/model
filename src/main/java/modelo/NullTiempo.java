package modelo;

public class NullTiempo extends AbstractTiempo{
	public void setFecha(int dia,int mes, int anio)
	{
		this.dia=dia;
		this.mes=mes;
		this.anio=anio;
	}
	
	public int getDia()
	{
		return 0;
	}
	
	
	public int getMes()
	{
		return 0;
	}
	
	public int getAnio()
	{
		return 0;
	}
	
	
	public NullTiempo ()
	{
		lugar=new Lugar("","","");
	}
	public String getAddress() {
		return "";
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTemp() {
		return "";
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	public String getMaxt() {
		return "";
	}
	public void setMaxt(String maxt) {
		this.maxt = maxt;
	}

	public String getConditions() {
		return "";
	}
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}
	public String getPais() {
		return "";
	}
	public void setPais(String pais) {
		lugar.setPais(pais);
	}
	public String getProvincia() {
		return "";
	}
	public void setProvincia(String provincia) {
		lugar.setProvincia(provincia);
	}
	public String getLocalidad() {
		return "";
	}
	public void setLocalidad(String localidad) {
		lugar.setLocalidad(localidad);
	}
	
	@Override
	public String toString() {
		return "JTiempo [Pais=" + lugar.getPais() + ", Provincia=" + lugar.getProvincia() + ", Localidad=" + lugar.getLocalidad()
			+ address + ", temp=" + temp + ", maxt=" + maxt
				+", conditions=" + conditions + "]";
	}

}
